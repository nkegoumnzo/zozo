
<!DOCTYPE html>
<html lang="FR">
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href=../Css/zoo.css />
        <title>ZOZO</title>
        </head>
    <body>
        <div>
            <header>
              <?php include 'header.php';?>
              <?php include 'slideshow.php';?>
    </header>
    
    <table>
    <caption>Panier</caption>
    <tr>
        <th scope="col"></th>
        <th scope="col">Nom</th>
        <th scope="col">Quantité</th>
        <th scope="col">Prix Unitaire</th>
    </tr>
    <?php
       if(isset($_SESSION['panier'])){
        $nbArticles=count($_SESSION['panier']['nom']);
        if ($nbArticles <= 0 || !isset($_SESSION['panier']['nom']))
        echo "<tr><td>Votre panier est vide </ td></tr>";
        else
        {
            for ($i=0 ;$i < $nbArticles ; $i++)
            
            {
            echo' <form method="POST" action="fonctionpanier.php">
            <input type="hidden" name="nom" value="', htmlspecialchars($_SESSION['panier']['nom'][$i], ENT_QUOTES, 'UTF-8'), '"/>
            <input type="hidden" name="img" value="', htmlspecialchars($_SESSION['panier']['img'][$i], ENT_QUOTES, 'UTF-8'), '"/>
            <input type="hidden" name="prix" value='.$_SESSION['panier']['prix'][$i].'/>
            <input type="hidden" name="id" value='.$_SESSION['panier']['id'][$i].'/>
            <input type="hidden" name="quantite" value='.$_SESSION['panier']['quantite'][$i].'/>
            <input type="hidden" name="action" value="suppression"/>
                <tr>
                <td><img src='.htmlspecialchars($_SESSION['panier']['img'][$i]).' alt="" width=100px height=100px /></ td>
                <td>'.htmlspecialchars($_SESSION['panier']['nom'][$i]).'</ td>
                <td>'.htmlspecialchars($_SESSION['panier']['quantite'][$i]).'</td>
                <td>'.htmlspecialchars($_SESSION['panier']['prix'][$i]).'$</td>
                <td><input type="submit" value="Supprimer"/></td>
                </tr>
                </form>';
            }

           echo' <tr><td colspan=\"2\"> </td>
            <td colspan=\"2\">
            Total : '.MontantGlobal().'$'.'
            </td></tr>

            <!--<tr><td colspan=\"4\">
            <form method="POST" action="panier.php">
            <input type="submit" value="Rafraichir"/>
            <input type="hidden" name="action" value="refresh"/>

            </td></tr>
            </form>-->';
        }
       }else{echo "<tr><td>Votre panier est vide </ td></tr>";}
    ?>
</table>
    </div>
<?php include 'footer.php';?>
<script type="text/javascript" src="../Javascript/javascript.js"></script>
</body>
</html>