-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 08, 2021 at 11:48 AM
-- Server version: 5.7.11
-- PHP Version: 5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zozo`
--
CREATE DATABASE IF NOT EXISTS `zozo` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `zozo`;

-- --------------------------------------------------------

--
-- Table structure for table `categorie`
--

CREATE TABLE `categorie` (
  `nom` varchar(99) NOT NULL,
  `idcat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categorie`
--

INSERT INTO `categorie` (`nom`, `idcat`) VALUES
('maillots', 1),
('chaussures de foot', 2),
('equipements', 3),
('accessoires', 4),
('sneakers', 5),
('soldes', 6),
('vetements', 7);

-- --------------------------------------------------------

--
-- Table structure for table `produit`
--

CREATE TABLE `produit` (
  `idprod` int(11) NOT NULL,
  `nom` varchar(99) NOT NULL,
  `prix` int(11) NOT NULL,
  `quantite` int(11) DEFAULT NULL,
  `idcat` int(11) NOT NULL,
  `image` varchar(99) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produit`
--

INSERT INTO `produit` (`idprod`, `nom`, `prix`, `quantite`, `idcat`, `image`) VALUES
(1, 'Barcelone extÃ©rieur', 200, 100, 1, '../images/barcelone.jpeg'),
(2, 'Barcelone domicile', 200, 100, 1, '../images/barcmaillot.jpeg'),
(3, 'Juventus domicile', 200, 100, 1, '../images/juventus.jpeg'),
(4, 'Dortmund domicile', 200, 100, 1, '../images/maillots.jpeg'),
(5, 'Bayern Ã©dition Pharell Williams', 200, 100, 1, '../images/bayernpharell.jpeg'),
(6, 'Chelsea Domicile', 200, 100, 1, '../images/chelsea.jpeg'),
(7, 'Predator mutator', 150, 100, 2, '../images/predat.webp'),
(8, 'Adidas nemezis', 125, 100, 2, '../images/nemezis.jpeg'),
(9, 'Adidas ghosted', 125, 100, 2, '../images/adidasghosted.jpeg'),
(10, 'Predator virtuoso', 125, 100, 2, '../images/predatorvirtuoso.jpeg'),
(11, 'Predator superlative', 125, 100, 2, '../images/predator.jpeg'),
(12, 'Puma Neymar', 125, 100, 2, '../images/pumaney.jpeg'),
(13, 'lbjxKM chosen 2', 125, 100, 2, '../images/mbappe.jpeg'),
(14, 'protÃ¨ge-tibias nike', 15, 100, 3, '../images/tibia.jpeg'),
(15, 'Gants gardien', 15, 100, 3, '../images/gants.jpeg'),
(16, 'gants jordan field-player', 15, 100, 3, '../images/gantsjordan.jpeg'),
(17, 'Air jordan 1', 150, 100, 5, '../images/jordan.webp'),
(18, 'Jordan retro', 145, 100, 5, '../images/jordan.jpeg'),
(19, 'Air jordan x psg', 105, 100, 5, '../images/jordanxpsg.jpeg'),
(20, 'Survetement', 80, 100, 7, '../images/survet.jpeg'),
(21, 'Sweet Jordan', 45, 100, 7, '../images/sweet.jpeg'),
(22, 'Tshirt Nike', 15, 100, 7, '../images/jd.jpeg'),
(23, 'Ensemble Nike', 105, 100, 7, '../images/jdf.jpeg'),
(24, 'Echelle de rythme', 15, 100, 4, '../images/echelle.jpg'),
(25, 'Ballon Nike', 25, 100, 4, '../images/ballon.jpeg'),
(26, 'Chasuble Nike', 15, 100, 4, '../images/chasuble.jpg'),
(27, 'Jalon de sport pvc', 10, 100, 4, '../images/jalon.jpg'),
(28, 'chaussettes Nike', 5, 100, 6, '../images/chaussettes.jpeg'),
(29, 'Polo psgxJordan', 105, 100, 6, '../images/psgxjordan.jpeg'),
(30, 'Nike superply', 55, 100, 6, '../images/superfly.jpeg'),
(31, 'Shaker', 15, 100, 4, '../images/shaker.jfif');

-- --------------------------------------------------------

--
-- Table structure for table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `nom` varchar(99) NOT NULL,
  `mdp` varchar(99) NOT NULL,
  `conn` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `utilisateur`
--

INSERT INTO `utilisateur` (`nom`, `mdp`, `conn`) VALUES
('byuu', 'bnbnbnbnbn', 0),
('Manuel ', 'Manuelito6', 0),
('nkegoumnzo', 'aaaaaaaa', 0),
('ser', 'bbbbbbbb', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`idcat`);

--
-- Indexes for table `produit`
--
ALTER TABLE `produit`
  ADD PRIMARY KEY (`idprod`);

--
-- Indexes for table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`nom`),
  ADD UNIQUE KEY `nom` (`nom`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
