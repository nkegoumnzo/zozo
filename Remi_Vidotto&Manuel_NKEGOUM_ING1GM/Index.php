<?php
// Start the session
session_start();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link rel="stylesheet" href=./Css/zoo.css />
        <title>ZOZO</title>
        </head>
    <body>
        <div>
            <header>
                <?php include './php/header.php';?>
                <?php include './php/slideshow.php';?>
        </div>
    </header>



    <section id= article >
        <article>
            <h1 style="text-align: center;"><u>SHOP BY COLLECTION</u></h1> 
        <aside id=img><!---->
            <ul id="ul_liste">
                <li><a href=./php/pagesneakers.php><img src=./images/sneakers.jpeg alt="" width="250px" height="250px" style="display: inline-block;"/> <span>Sneakers</span></a></li>
                <li> <a href=./php/pagechaussuresfoot.php > <img src=./images/predator.jpeg alt="" width="250px" height="250px" style="display: inline-block;"/> <span>Chaussures de Football</span></a></li>
                <li> <a href=./php/pagemaillots.php ><img src=./images/maillots.jpeg alt="" width="250px" height="250px" style="display: inline-block;"/> <span>Maillots de Football</span></a></li>
                <li> <a href=./php/pageequipements.php><img src=./images/vetements.jpg alt="" width="500px" height="250px" style="display: inline-block;"/> <span>Equipements de Football</span></a></li>
                <li> <a href=./php/pagevetements.php ><img src=./images/street.jpeg alt="" width="500px" height="250px" style="display: inline-block;"/> <span>Streetwear</span></a></li>
                <li><a href=./php/pagesoldes.php><img src=./images/promo.jfif alt="" width="800px" height="250px" style="display: inline-block;"/><span>Promotions</span></a></li>
                <li> <a href=./php/pageaccessoires.php ><img src=./images/accessoires.jpeg alt="" width="500px" height="250px" style="display: inline-block;"/> <span>Accessoires</span></a></li>
            </ul>
         </aside>
        </article>
    </section>
    <hr>
    <section>
        <article >
            <h1><u>NEWS</u></h1>
            <h2>PUMA</h2>
                    <p>
                    PUMA l'a encore fait. La nouvelle Future Z Game On est enfin disponible pour tout le monde sur Unisport. Tu peux l'avoir avant les autres, donc fais-toi plaisir dès maintenant ! Sois au centre de l'attention avec ce nouveau coloris.
                    Neymar et PUMA reviennent avec une nouvelle chaussure. On avait pu assister à la sortie de la Future Z en édition limitée, l'an dernier, dans un coloris blanc, jaune et noir. Future Z est de retour dans un nouveau coloris jaune et noir et elle est disponible pour tout le monde, sur Unisport, bien évidemment.</p></li>
            <h2>LBJ x KM</h2>
                <p>Nike démarre 2021 en folie ! En faisant appel à deux de ses meilleurs athlètes, Nike présente la collection LeBron James x Kylian Mbappé “Chosen 2”. Disponible dès le 9 janvier sur Unisport bien évidemment ! Ce n'est pas la première fois que Nike nous fait le coup de combiner le basket avec le foot. Il n'y a pas longtemps, Nike sortait le troisième maillot Paris Saint Germain x Jordan, ainsi que la célèbre collection lifestyle PSG x Jordan. La collab PSG x Jordan, qui a vu le jour en 2018, continue de plaire dans le monde entier au fil des années.
                    la nouvelle Nike Mercurial Superfly 7 LBJ x KM “Chosen 2”. Une chaussure qui est inspirée de l'amitié entre la star du basket américain, LeBron James et Kylian Mbappé mais aussi une référence au basketball qui est devenu populaire en France ces dernières années et qui est une autre passion pour Kylian. Cette paire, c'est bien évidemment du jamais vu et elle sera portée par Kylian Mbappé en personne.
                </p>
                <h2>SUPE BOWL</h2>
                <p>Vainqueur de son septième Super Bowl, Tom Brady a annoncé qu'il comptait bien revenir la saison prochaine.

                    Tom Brady l'a fait. Alors que de nombreux doutes existaient quant à la possibilité de le voir réussir ailleurs qu'à New England, TB12 a ramené le Super Bowl à Tampa Bay dès sa première saison en Floride. Après 20 ans et six titres de champion avec le légendaire entraîneur Bill Belichick à New England, Tom Brady aura permis aux Buccaneers de soulever le Trophée Lombardi pour la première fois depuis 2003. Et l'ancienne star des Patriots n'est toujours pas rassasiée...
                    « On revient l'année prochaine ! »
                    
                    Après avoir dominé les Chiefs de manière indiscutable (31-9), les Buccaneers sont devenus la première équipe de l'histoire de la NFL à disputer et à remporter le Super Bowl dans leur propre stade, le Raymond James Stadium. Au terme de la rencontre, et au moment de fêter son septième titre de champion, Tom Brady a affirmé qu'il ne comptait pas prendre sa retraite au micro de CBS : « On revient l'année prochaine ! Vous pouvez en être sûrs ! » À 44 ans, TB12 est prêt à aller chercher un huitième sacre et cimenter sa place dans l'histoire de la NFL !</p>
        </article>
        <aside>
            <img src=./images/neymar.jpeg alt=""  width="600px" height="700px">
            <img src=./images/lbj.jpeg alt=""  width="600px" height="300px">
        </aside>
    </section>

    <hr>
    <div id=article>
        <h1 style="text-align: center;font-size:large;"><u>LES MEILLEURES VENTES</u></h1>
        <aside id=img><!---->
            <ul id="ul_liste">
                <li><img src=./images/mbappe.jpeg alt="" width="250px" height="250px" style="display: inline-block;"/> <span> LBJ x KM “Chosen 2: 125$</span></a></li>
                <li> <img src=./images/gantsjordan.jpeg alt="" width="250px" height="250px" style="display: inline-block;"/> <span>Gants Jordan field-player : 15$</span></a></li>
                <li><img src=./images/barcelone.jpeg alt="" width="250px" height="250px" style="display: inline-block;"/> <span> Barcelone extérieur: 200$</span></a></li>
                <li><img src=./images/jordan.webp alt="" width="250px" height="250px" style="display: inline-block;"/> <span>Air Jordan 1: 150$</span></a></li>
                <li> <img src=./images/tibia.jpeg alt="" width="500px" height="250px" style="display: inline-block;"/> <span>protège-tibia nike: 15$</span></a></li>
                <li><img src=./images/predat.webp alt="" width="800px" height="250px" style="display: inline-block;"/> <span>Predator mutator 20+ : 125$:</span></a></li>
                <li><img src=./images/chaussettes.jpeg alt="" width="500px" height="250px" style="display: inline-block;"/> <span>chaussettes nike : 5$</span></a></li>
                <li><img src=./images/sweet.jpeg alt="" width="500px" height="250px" style="display: inline-block;"/> <span>Sweet Jordan: 45$</span></a></li>    
            </ul>
         </aside>
         <hr>
         <h1><u>Neymar x puma</u></h1>
        <video width="600px" height="400px" class=centered >
            <source src=./video/puma.mp4 type="video/mp4">
            Video is not supported by your browser
         </video>
    </div>

</div>
<?php include './php/footer.php';?>
<script type="text/javascript" src="./Javascript/javascript.js"></script>
</body>
</html>